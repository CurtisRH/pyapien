#!/usr/local/bin/python3
#
# PyUnit Test for Py Apien
#
# Name: test_py_apien.py
# Developer: Curtis Hendricks
# Date: 27 June 2015
# Version: 1.0
#

"""
Unit test for py_apien class, a python class wrapper for Human Development 
Report data APIEN API.
"""

import unittest
import json
import pycurl
from io import BytesIO
from py_apien import PyApien

class TestPyApien(unittest.TestCase):
    
    def test_data_retrieval(self):
        '''
        Retrieve the data directly via API using the query URL to compare with the data retrieved by 
        using an object of the PyApien class.  
        '''
        country_dict = {"COL": "Colombia", "JAM": "Jamaica"}
        indicator_dict = {137506: "HDI: Human development index (HDIg) value"}
        years_list = [1990, 2010]
        query = "http://ec2-52-1-168-42.compute-1.amazonaws.com/version/1/country_code/{0}/indicator_id/{1}/year/{2}?structure=ciy".format(
                ','.join(country_dict.keys()), ','.join([str(i) for i in indicator_dict.keys()]),
                ','.join([str(y) for y in years_list]))

        # Retrieve data directly from APIEN API
        apien_buffer = BytesIO()
        fetch = pycurl.Curl()
        fetch.setopt(fetch.URL, query)
        fetch.setopt(fetch.WRITEDATA, apien_buffer)
        fetch.perform()
        fetch.close()
        data_dict = dict(json.loads(apien_buffer.getvalue().decode('iso-8859-1')))
        
        # Create PyApien object
        HDR_Data = PyApien(countries=country_dict.keys(), indicators=indicator_dict.keys(), years=years_list)
        
        # See if the queries are the same
        self.assertEqual(query, HDR_Data.get_query_string(), "Query strings are not the same.")
        # See if the data is the same
        self.assertEqual(data_dict['indicator_value'], HDR_Data.get_data_dict(), "Data is not equal.")
        # Test storage of query parameters
        self.assertEqual(country_dict, HDR_Data.get_country_code_dict(), 
                         "List of query countries codes is incorrect.")
        self.assertEqual([i for i in indicator_dict.keys()], [int(i) for i in HDR_Data.get_indicator_code_dict().keys()], 
                         "List of query indicator ids is incorrect.")
        self.assertEqual(years_list, HDR_Data.years['years'], 
                         "List of query years is incorrect.")
        self.assertEqual('', HDR_Data.get_error_message())
        self.assertFalse(HDR_Data.in_error_state)
        
#################################
# Need to create a test here for input of incorrect data
#
#   def test_incorrect_data(self):
#       '''
#       Test if "in_eror_state" parameter and "error_message" 
#       is set when APIEN API returns an error message.
#       '''
#       # Incorrect Indicator id
#      
#       # Incorrect country code
#       
#       # Incorrect year value
#
################################

    def test_object_reset(self):
        '''
        Test the reset feature for the object.
        '''
        HDR_Data = PyApien(indicators=[137506, 105906], countries=['JAM', 'COL'], years=[2010])
        self.assertEqual(84, int(HDR_Data.data['COL']['105906']['2010']))
        HDR_Data.reset(indicators=[137506, 105906], countries=['AFG', 'USA'], years=[2010])
        self.assertFalse('COL' in HDR_Data.data.keys(), 'Colombia, "COL", is still in the dataset')
        self.assertEqual(0.908, float(HDR_Data.data['USA']['137506']['2010']))
    
if __name__ == "__main__":
    unittest.main()

# README #

### What is this repository for? ###

* Summary: 
__PyApien__ is a python class wrapper for the Human Deveopment Report's dataset API [__APIEN__](https://github.com/HDRData/api). 

* PyApien provides a convenient python class wrapper for the APIEN API that creates and executes a query and stores the result in an instantiated python object that provides easy access to the data within a python application.

* APIEN is an API to access the open and freely available _Human Development Report_ (HDR) data published by the _Human Development Report Office_ (HDRO) of the _United Nations Development Programme_ (UNDP). APIEN decsription from the [githib site](https://github.com/HDRData/api):

>This REST API was originally built in order to allow users to query the UNDP 
>Human Development Report Office (HDRO) database and return structured results 
>in json format by ISO3 Country Code, Indicator ID and Year.

* Version: 1.0


### How do I get set up? ###

* Summary of set up
Copy the python file, py\_apien.py to your working directory for your script and import the PyApien class:

		from py_apien import PyApien
	

	 
* Dependencies : The following python libraries are used - json, pycurl, BytesIO. Program was written using Pyhton3 grammar and has not been tested with Python2 interpreter. In order to automatically install dependencies, run the following from the module root directory:
python setup.py develop

### How to run tests ###
* Unit test program included, test_py_apien.py.

### Who do I talk to? ###

* Repo owner

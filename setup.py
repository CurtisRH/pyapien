"""
Checks for dependencies and installs if required.
In order to run: python setup.py develop
"""
from setuptools import setup

setup(name='PyApien',
      version='0.1',
      description='Summary: PyApien is a python class wrapper for the Human Deveopment Report dataset API APIEN.',
      url='https://bitbucket.org/CurtisRH/pyapien',
      author='Curtis Hendricks',
      license='MIT',
      install_requires=[
          'requests',
      ],
      zip_safe=False)

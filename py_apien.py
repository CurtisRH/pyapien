#!/usr/local/bin/python3
#
# Python class for APIEN API
#
# Name: py_apien.py
# Developer: Curtis Hendricks
# Date: 27 June, 2015
# Version: 1.0
#

"""
Python class wrapper to manage the data retrieved Human Development Report Office 
(HDRO) APIEN API. The APIEN API was developed by the Human Development Report 
Office (HDRO) of the United Nations Development Programme (UNDP) to access data 
from their yearly publicized Human Development Report (HDR).
"""

import json
import requests

COUNTRY_CODE = "country_code"
INDICATOR_ID = "indicator_id"
YEAR = "year"
STRUCTURE = "structure"
GZIP = "gzip"
CIY = "ciy"
YIC = "yic"
YCI = "yci"
IYC = "iyc"
ICY = "icy"

I_VALUES = 'indicator_value'
I_NAMES = 'indicator_name'
C_NAMES = 'country_name'

APIENURL = "http://ec2-52-1-168-42.compute-1.amazonaws.com/version/1"


class PyApien:
    '''
    Object wrapper for data retrieved from APIEN API for UNDP HDR data.
    '''
    
    def __init__(self, dump=False, countries=None, indicators=None, years=None, 
                 struct=CIY):
        '''
        Create a PyApien object with no data, the entire dataset (dump=True), or
        specific results related to specific countries (countries = [countries list])
        and/or HDR indicators (indicators = [indicator list]) and/or specific years
        (years = [list of years]). Output can be grouped by any combination of: country 
        (C), indicator (I), year(Y).
        NOTE: Due to limitations on the length of number of characters in a URL request, 
        the number of indicators and countries will each be limited to 20 each.
        DANGER: Only the first 20 indicators and the first 20 countries will be used in
        any query. All additional indicators or countries will be truncated and ignored.   
        '''
        self.query = ''
        self.data = {}
        self.country_names = {}
        self.indicator_names = {}
        self.years = {}
        self.in_error_state = False
        self.error_message = ''
        if dump or countries or indicators or years:
            if countries:
                if len(countries) > 20:
                    countries = countries[:20]
            if indicators:
                if len(indicators) > 20:
                    indicators = indicators[:20]
            self.query = self.__build_query(dump, countries, indicators, 
                                          years, struct)
            apien_data = self.__query_apien(self.query)
            if not self.in_error_state:
                self.data = apien_data[I_VALUES]
                self.country_names = apien_data[C_NAMES]
                self.indicator_names = apien_data[I_NAMES]
                if years:
                    self.years['years'] = years
    
    def __repr__(self):
        print_out = ""
        if self.indicator_names:
            print_out += 'Indicator names:\n'
            print_out += json.dumps(self.indicator_names, sort_keys=True, indent=2) + "\n"
        if self.country_names:
            print_out += 'Country names:\n'
            print_out += json.dumps(self.country_names, sort_keys=True, indent=2) + "\n"
        if self.years:
            print_out += 'Years:\n'
            print_out += json.dumps(self.years, sort_keys=True, indent=2) + "\n"
        if self.data:
            print_out += 'Data:\n'
            print_out += json.dumps(self.data, sort_keys=True, indent=2) + "\n"
            
        return print_out
    
    def get_country_code_dict(self):
        '''
        Returns the country code dictionary property which contains the country codes that 
        were included in the query. Country codes strings are keys with the country name 
        strings as values. Returns an empty dictionary if no country codes were included.
        '''
        return self.country_names
    
    def get_indicator_code_dict(self):
        '''
        Returns the indicator code dictionary property which contains the indicator codes 
        that were included in the query. Indicator code integers are keys with the indicator 
        code strings as values. Returns an empty dictionary if no indicator codes were included.
        '''
        return self.indicator_names
    
    def get_years_list(self):
        '''
        Returns a list containing the years that were included in the query. If none were
        included, an empty list is returned.
        '''
        years = []
        if self.years:
            years = self.years['years']
        return years
        
    def get_query_string(self):
        '''
        Returns the full URL string used for the query.
        '''
        return self.query
    
    def get_data_dict(self):
        '''
        Returns a dictionary containing the data retrieved from the query. Returns 
        an empty dictionary if there is no data.
        '''
        return self.data
    
    def get_error_state(self):
        '''
        Returns the error state of the current object. True only if there was an issue
        retrieving data via the APIEN API.
        '''
        return self.in_error_state
    
    def get_error_message(self):
        '''
        Returns any error message associated with the error state of the current object.
        If there is no error, error message is an empty string.
        '''
        return self.error_message
            
    def reset(self, dump=False, countries=None, indicators=None, 
              years=None, struct=CIY):
        '''
        Resets the contents of the object.
        CAUTION - All current data for the object will be replaced!
        '''
        self.__init__(dump=dump, countries=countries, indicators=indicators, years=years, struct=struct)
    
    
    def symbol_definition_print(self):
        '''
        Returns a string containing the contents of the data property including the 
        full text names of the country codes and the indicator codes.
        '''
        str_data = json.dumps(self.data)
        for key, val in self.country_names.items():
            str_data = str_data.replace(key, '['+key+'] '+val)
        for key, val in self.indicator_names.items():
            str_data = str_data.replace(key, '['+key+'] '+val)
        
        dct_output = json.loads(str_data)
        
        return json.dumps(dct_output, indent=2, sort_keys=True)
    
    def output_file(self, filename):
        '''
        Creates an output file named 'filename' to the current directory.
        '''
        file_query = self.__build_file_query(filename)
        try:
            file_obj = open(filename, 'w')
        except Exception as e:
            print('File {0} cannot be created due to exception.'.format(filename))
            print(e)
        json.dump(json.loads(self.__query_apien(file_query)), 
                  file_obj, sort_keys=True, indent=2)
        
    def __query_apien(self, query):
        '''
        Executes the query URL and returns the data as a dict.
        '''
        r = requests.get(query)
        apien_buffer = r.json()
        return apien_buffer
        
    
    def __build_query(self, dump=True, countries=None, indicators=None, 
                    years=None, struct=CIY):
        '''
        Returns a URL query string for results to standard output. If dump is 
        true (default), all other parameters except 'struct' are ignored.
        '''
        queryURL_head = APIENURL
        if dump:
            return queryURL_head + '?' + STRUCTURE + '=' + struct
        
        if countries:
            queryURL_head += '/'+ COUNTRY_CODE + '/' + ','.join(countries)
            
        if indicators:
            str_indicators = [str(i) for i in indicators]
            queryURL_head += '/' + INDICATOR_ID + '/' + ','.join(str_indicators)
            
        if years:
            str_years = [str(y) for y in years]
            queryURL_head += '/' + YEAR + '/' + ','.join(str_years)
            
        return queryURL_head + '?' + STRUCTURE + '=' + struct
    
    def __build_file_query(self, filename):
        '''
        Returns a query string that adds the parameter to the current query to send the
        results to a file with name 'filename'.
        '''
        query = self.query;
        if query.find('/?structure') > -1:
            query = query.replace('?structure', filename + '?structure')
        else:
            query = query.replace('?structure', '/' + filename + '?structure')
        
        return query

if __name__ == "__main__":
    
    HDI_data = PyApien(indicators=[105906, 20206], 
                       countries=['USA', 'AFG', 'JAM', 'COL'])
    if HDI_data.in_error_state:
        print("Error with query:", HDI_data.query)
        print("Error message:", HDI_data.get_error_message())
    else:
        print(HDI_data.symbol_definition_print())
    
